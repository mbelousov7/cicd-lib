# gitlab cicd libs

This repo contains necessary gitlab-cicd libs(yml) and scripts for projects&pipelines

## repository structure

Files description
 
| File | description |
|:-------|:---------|
| **docs/\<instruction name\>.md** | some readme, desxriptions and instructions | 
| **utils/init_repo.py** |  python script for init repo configuration | 
| **includes/component/default.yml** | default configuration and stages an for component-repos | 
| **includes/component/docker.yml** | contains all necessary stages for component-repos with docker image output | 
| **includes/component/bams.yml** | contains all necessary stages for component-repos which stores artifacts Binary Artifact Management System (BAMS) | 
| **includes/component/s3.yml** | contains all necessary stages for component-repos which stores artifacts in s3 | 
| **includes/component/all-\<lib: component type\>.yml** | lib-stack which contains combinations of libs(Builds, scans, tests and deploys) for particular component type | 
| **includes/stack/\<stack name\>-sarvice-stack.yml** | lib-trigger which contains configuration to trigger stack project build | 
| **includes/lib/\<lib name\>.yml** | reusable libs which is used in other Lib-jobs | 
| **includes/terraform-deploy.yml** | lib for deploy terrafrom-service-stack's | 
| **includes/terraform-libs.yml** | lib for deploy terrafrom-lib's (modules) | 


## documentation

here the links on FAQ info

- [ component repos info ](docs/components.md)
- [ stack repos info ](docs/terrafrom-deploy.md)






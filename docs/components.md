# Components repos 

## cicd configuration for components repos

to configure gitlab-ci pipeline for component repo you need:

0) create branch and MR to main branch 
1) add README.md file at least with some description of component
2) add .gitignore file (or copy from other component repo)
3) add CHANGELOG.md (see examle in any other component repo) and first version info
4) add .gitlab-ci.yml

``` yaml
variables:
# main component repo  variables
  COMPONENT_NAME: <component name, must be the same as repo name>
  SONAR_PROJECT_KEY: <component sonar id>
# optional component repo variables
  CONFIG_FILE_TO_UPDATE: <path to some config or code file to update [!! VERSION ] pattern in this file>
  PYTHON_IMAGE: <python image path IF repo contains python code to build>
  ARTIFACT_FILE: <path to python source file if repo contains python source code to s3 upload >
  GRADLE_IMAGE: <gradle image path IF repo contains gradle code to build>
  MAVEN_IMAGE: <maven image path IF repo contains maven code to build>

include:
  - project: $CICD_LIB_PROJECT
    ref: develop
    file:
      - '/includes/component/[choose appropriate to your component type lib all-\<lib: component type\>].yml'
      - '/includes/stack/[your terraform stack name]-service-stack.yml'
```

5) check pipelene status in gitlab-ci

## hotfix

if you need to build hotfix version of component:

1. create hotfix/<some descr> branch (Branch name should start with hotfix/), from a given version tag using `git checkout -b hotfix/my-hotfix v1.2.3` (where `v1.2.3` is the version you're hotfixing)
2. add <new version> into changelog
3. add and commit and push your changes
4. check build status in component repo pipeline
5. manually update the component version in in stack repo for necessary environment and run deploy
 [ detailes see here ](docs/terraform-stack.md)
6. check deployment status in stack repo pipeline after

hotfix ci-utils activated if IS_HOTFIX env var exist, and [ hotfix logic implemented here ](includes/prerequisites.yml)

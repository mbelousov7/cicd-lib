import gitlab
import os
import argparse
import sys

# https://python-gitlab.readthedocs.io/en/stable/api-objects.html

if __name__ == "__main__":
    token = os.environ.get('GITLAB_ACCESS_TOKEN')
    url = os.environ.get('GITLAB_URL')

    if token is None:
        print("error: couldn't find GitLab token")
        print("make sure your personal access token is present in the 'GITLAB_ACCESS_TOKEN' environment variable")
        exit(1)
    if url is None:
        token = "https://gitlab.com"
    
    parser = argparse.ArgumentParser(description='Initializes a GitLab repository with our default settings and variables')
    parser.add_argument('--gitlab-project-id', required=True, type=str, help='the project ID, you can find it on the project page under the name')
    parser.add_argument('--aws-ppe-account-id', required=True, type=str, help='the account ID for the AWS PPE account')
    parser.add_argument('--aws-prod-account-id', required=True, type=str, help='the account ID for the AWS PPE account')

    if len(sys.argv) == 1:
        parser.print_help()
        exit(1)

    args = parser.parse_args()

    gl = gitlab.Gitlab(url=url, private_token=token)

    try:
        project = gl.projects.get(args.gitlab_project_id)
    except gitlab.GitlabGetError:
        print(f"error: project with ID {args.gitlab_project_id} not found; this could also be caused by invalid credentials")
        print("make sure your personal access token is present in the 'GITLAB_ACCESS_TOKEN' environment variable")
        exit(1)

    print(f"Initializing {project.name}")

    print("Creating variables")
    aws_variables = [
        "PPE_AWS_ACCESS_KEY_ID",
        "PPE_AWS_SESSION_TOKEN",
        "PPE_AWS_SECRET_ACCESS_KEY",

        "PROD_AWS_ACCESS_KEY_ID",
        "PROD_AWS_SESSION_TOKEN",
        "PROD_AWS_SECRET_ACCESS_KEY"
    ]

    for variable in aws_variables:
        project.variables.create({
            "key": variable,
            "value": "dummy-value",
            "masked": True,
            "protected": False
        })

    project.variables.create({
        "key": "PPE_AWS_ACCOUNT_ID",
        "value": args.aws_ppe_account_id,
        "masked": False,
        "protected": False
    })

    project.variables.create({
        "key": "PROD_AWS_ACCOUNT_ID",
        "value": args.aws_prod_account_id,
        "masked": False,
        "protected": False
    })

    print("Creating project access token")
    project_access_token = project.access_tokens.create({
        "name": "ci-token",
        "scopes": [
            "api",
            "read_api",
            "read_repository",
            "write_repository",
            "read_registry",
            "write_registry"
        ]
    })

    project.variables.create({
        "key": "PROJECT_ACCESS_TOKEN",
        "value": project_access_token.token,
        "masked": True,
        "protected": False
    })

    print("Updating repository settings")
    project.container_expiration_policy_attributes = {
        'cadence': '1d',
        'enabled': True,
        'keep_n': None,
        'older_than': '7d',
        'name_regex': 'tmp-.*',
        'name_regex_keep': None,
    }

    project.only_allow_merge_if_pipeline_succeeds = True
    project.only_allow_merge_if_all_discussions_are_resolved = True
    project.squash_option = "default_on"
    project.save()

    project.approvalrules.create({
        'name': 'min_approvals_rule',
        'approvals_required': 1
    })

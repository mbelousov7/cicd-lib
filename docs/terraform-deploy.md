# Terraform-deploy stack repos 

### CICD diagram

![CICD diagram](CICD.png)


### terrafrom stack structure

![terrafrom stack structure](terraform_stack.png)

### local terraform plan & apply

to check plan in dev:

0. Prerequisites
- Install terraform binary
- AWS Authentication
copy keys into .env
AWS_ACCESS_KEY_ID=******
AWS_DEFAULT_REGION=us-east-1
AWS_SECRET_ACCESS_KEY=******
TFSTATE_BUCKET=********
``` bash
export $(cat .env | xargs)
```


1. Preparing env ( this and next run in bash)
- Run
``` bash
export TF_VAR_ENV="dev"
git checkout origin/infra-${TF_VAR_ENV} -- versions.json
terraform init -backend-config="env/${TF_VAR_ENV}.conf"
terraform workspace select ${TF_VAR_ENV}
```

2. Terraform plan check
- Run
``` bash
terraform plan -var-file "env/${TF_VAR_ENV}.tfvars" -var="components_versions=$(cat versions.json)"
```

3. Terraform apply
- Run
``` bash
terraform apply -var-file "env/${TF_VAR_ENV}.tfvars" -var="components_versions=$(cat versions.json)"
```

## pipeline flow for stack-release repos

### How to change  terraform code version
1) create new branch **my-branch-name** from **develop**
2) add commit, add new version in CHANGELOG.md 
3) push changes in to **my-branch-name**
4) create new MR **my-branch-name** -> **develop**
5) wait success in MR pipeline status
6) Approve & Merge MR (+ review terraform plan)
7) wait success in **develop** pipeline status
8) check status of the new project tag(value the same as in CHANGELOG.md)
9) run stack pipeline from **infra-dev** branch to deploy new version of terraform-stack in dev
10) wait success in **infra-dev** pipeline status

### How to deploy terraform stack in DEV env
if you need to apply last versions of stack and components
1) run stack pipeline from **infra-dev** branch ( *TF_DEPLOY_DEV_LATEST_VERSIONS* value should be *true*)
2) wait success in **infra-dev** pipeline status

if you need to apply not last versions of stack and components
1) make changes in **infra-dev/versions.json**  
   by default last versions of stack&components will be deployed
2) push changes and wait success in **infra-dev** pipeline status

### How to deploy terraform stack in PREPROD env
to deploy new versions of stack&components

- Prepare release config
1) commit&push new versions into versions.json in **infra-preprod** branch
2) wait success in **infra-preprod** pipeline status

- run deploy
1) run stack pipeline from **infra-preprod** branch, define next pipeline configuration:  
   **TF_DEPLOY_PPE_PROD**: *true* 
2) wait success in **infra-preprod** pipeline status

### How to deploy terraform stack in PROD env
to deploy new versions of stack&components

- Prepare release config
1) create new branch **release-1.2.3** from **infra-prod**
2) put new versions in versions.json
3) push changes in to **release-1.2.3**
4) create new MR **release-1.2.3** -> **infra-prod**
5) wait success in MR pipeline status
6) Approve & Merge MR (+review terraform plan)
7) wait success in **infra-prod** pipeline status

- run deploy
1) run stack pipeline from **infra-prod** branch, define next pipeline configuration:  
   **TF_DEPLOY_PPE_PROD**: *true* 
2) wait success in **infra-prod** pipeline status